﻿using System;
using System.Linq;
using Bitz.Engine.Core.Debug;
using Bitz.Engine.Core.Debug.Remoting;
using Sbatman.Serialize;

namespace Bitz.Tools.Watcher
{
    public static class MessageProcessor
    {
        public static void ProcessMessage(RemoteMessage message)
        {
            switch ((DebugService.DebugMessageIDs)message.ID)
            {
                case DebugService.DebugMessageIDs.SERVICESTATUS:
                    // Console.Clear();
                    int targetWidth = Console.WindowWidth;
                    Console.SetCursorPosition(0, 0);
                    Packet serviceData = Packet.FromByteArray(System.Convert.FromBase64String(message.Message));
                    foreach (String s in serviceData.GetObjects().Cast<String>())
                    {
                        Console.WriteLine(s.Replace(',', ' ').PadRight(targetWidth-6));
                    }

                    break;
            }
        }
    }
}
