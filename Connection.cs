﻿using System;
using System.Net.Sockets;
using Bitz.Engine.Core.Debug.Remoting;
using Sbatman.Networking.Server;
using Sbatman.Serialize;

namespace Bitz.Tools.Watcher
{
    class Connection : ClientConnection
    {
        public Connection(TcpClient newSocket) : base(newSocket)
        {
        }

        protected override void OnConnect()
        {
        }

        protected override void OnDisconnect()
        {
        }

        protected override void ClientUpdateLogic()
        {
            foreach (Packet packet in GetOutStandingProcessingPackets())
            {
                Object[] objects = packet.GetObjects();
                RemoteMessage remoteMessage = new RemoteMessage()
                {
                    ID = (Int16)objects[0],
                    Message = (String)objects[1]
                };
                MessageProcessor.ProcessMessage(remoteMessage);
            }
        }

        protected override void HandelException(Exception e)
        {
            Int32 g = 7;
        }
    }
}
