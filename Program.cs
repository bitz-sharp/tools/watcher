﻿using System;
using System.Net;
using System.Threading;
using Sbatman.Networking.Server;

namespace Bitz.Tools.Watcher
{
    class Program
    {
        private static void Main(String[] args)
        {
            BaseServer server = new BaseServer();
            server.Init(new IPEndPoint(IPAddress.Any, 4565), typeof(Connection));
            server.StartListening();
            while (server.IsListening()) Thread.Sleep(20);
        }
    }
}
